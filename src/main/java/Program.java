import static java.lang.System.out;

class Program {

    void main() throws Exception {
        var prefix = "it";
        out.println(prefix + " works");
    }

    public static void main(String[] args) throws Exception {
        new Program().main();
    }
}