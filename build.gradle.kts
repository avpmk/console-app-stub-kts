plugins {
    application
}

group = "ru.petrovandrei"
version = "0.1"

java {
    sourceCompatibility = JavaVersion.VERSION_1_10
    targetCompatibility = JavaVersion.VERSION_1_10
}
tasks.withType<JavaCompile> {
    options.encoding = "UTF-8"
}

application.mainClassName = "Program"
tasks.withType<Jar> {
    manifest.attributes["Main-Class"] = application.mainClassName
}

repositories {
    mavenCentral()
}

dependencies {
    //compileOnly("org.projectlombok", "lombok", "1.16.20")
    testImplementation("junit", "junit", "4.12")
}
